import type {DatePickerProps} from 'antd';
import {DatePicker} from 'antd';
import React, { useState } from 'react';
import moment from "moment";
import './App.css';
import 'antd/dist/antd.css';




const App: React.FC = () => {
    const [day, setDay] = useState('')
    const [month, setMonth] = useState('')
    const [year, setYear] = useState('')


    const onChange: DatePickerProps['onChange'] = (date, dateString) => {
        console.log(dateString);
        console.log(moment(date).format("YYYY"))
        console.log(moment(date).format("M"))
        console.log(moment(date).format("D"))
        setDay(moment(date).format("D"))
        setMonth(moment(date).format("M"))
        setYear(moment(date).format("YYYY"))

    };

    return (
        <div className="App">
            <div>
                <h2>Введите дату рождения</h2>
                <DatePicker onChange={onChange}/>
                {year &&
                    <h2> Ваш день рождения: {day}/{month}/{year} </h2>
                }
            </div>
        </div>
    );
}

export default App;
